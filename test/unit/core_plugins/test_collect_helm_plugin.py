"""
Test module for CollectHelmPlugin class
"""

# pylint: disable=protected-access
# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument

from __future__ import annotations

import test

from pathlib import Path
from subprocess import CompletedProcess
from typing import Callable, Optional

import pytest

from hoppr_cyclonedx_models.cyclonedx_1_4 import Component
from pytest import FixtureRequest, MonkeyPatch

import hoppr.core_plugins.collect_helm_plugin
import hoppr.plugin_utils
import hoppr.utils

from hoppr.configs.credentials import Credentials
from hoppr.context import Context
from hoppr.core_plugins.collect_helm_plugin import CollectHelmPlugin
from hoppr.hoppr_types.cred_object import CredObject
from hoppr.hoppr_types.manifest_file_content import Repository
from hoppr.hoppr_types.purl_type import PurlType
from hoppr.result import Result

sbom_path = Path(test.__file__).parent / "resources" / "bom" / "int_helm_bom.json"


@pytest.fixture(name="component")
def component_fixture(request: FixtureRequest):
    """
    Test Component fixture
    """
    purl = request.param if hasattr(request, "param") else "pkg:helm/something/else@1.2.3"
    return Component(name="TestHelmComponent", purl=purl, type="file")  # type: ignore


@pytest.fixture(scope="function")
def load_file_fixture(request: FixtureRequest) -> Callable:
    """
    Test yaml.safe_load fixture
    """

    def _load_file(input_file_path: Path) -> list | dict:
        data: list[dict[str, str]] = [
            dict(url="https://charts.hoppr.com/hoppr", name="hoppr"),
            dict(url="https://charts.hoppr.com/stable", name="stable"),
        ]

        if request.param == "raise":
            return data

        return dict(repositories=data)

    return _load_file


@pytest.fixture(scope="function")
def context_fixture(
    context_fixture: Context, load_file_fixture: Optional[dict[str, str]], monkeypatch: MonkeyPatch
) -> Context:
    """
    Test Context fixture
    """
    monkeypatch.setattr(target=Path, name="exists", value=lambda self: True)
    monkeypatch.setattr(target=Path, name="read_text", value=lambda self: "")
    monkeypatch.setattr(target=hoppr.core_plugins.collect_helm_plugin.utils, name="load_file", value=load_file_fixture)

    return context_fixture


@pytest.fixture(scope="function", params=[dict(plugin_class=CollectHelmPlugin)])
def plugin_fixture(
    plugin_fixture: CollectHelmPlugin, context_fixture: Context, monkeypatch: MonkeyPatch, tmp_path: Path
) -> CollectHelmPlugin:
    """
    Override and parametrize plugin_fixture to return CollectHelmPlugin
    """
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=lambda comp: ["https://somewhere.com"])

    plugin_fixture.context.collect_root_dir = str(tmp_path)
    plugin_fixture.context.manifest.consolidated_repositories = {
        PurlType.HELM: [Repository(url="https://somewhere.com", description="")]
    }

    return plugin_fixture


@pytest.mark.parametrize(
    argnames=["completed_process_fixture", "config_fixture"],
    argvalues=[(dict(returncode=0), dict(helm_command="helm")), (dict(returncode=1), dict(helm_command="helm"))],
    indirect=True,
)
# pylint: disable=too-many-arguments
def test_collect_helm(
    plugin_fixture: CollectHelmPlugin,
    config_fixture: dict,
    completed_process_fixture: CompletedProcess,
    run_command_fixture: CompletedProcess,
    find_credentials_fixture: CredObject,
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test collect method
    """
    monkeypatch.setattr(target=Credentials, name="find_credentials", value=find_credentials_fixture)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)

    if completed_process_fixture.returncode == 0:
        assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"
    else:
        assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
        assert collect_result.message.startswith(
            "Failure after 3 attempts, final message Failed to download else version 1.2.3 helm chart"
        )


@pytest.mark.parametrize(
    argnames=["context_fixture", "load_file_fixture"],
    argvalues=[(dict(strict_repos=False), "load"), (dict(strict_repos=False), "raise")],
    indirect=True,
)
def test_helm_no_strict(
    plugin_fixture: CollectHelmPlugin,
    context_fixture: Context,
    load_file_fixture: list[dict[str, str]],
    monkeypatch: MonkeyPatch,
):
    """
    Test collect method: --no-strict flag
    """
    monkeypatch.setattr(target=hoppr.utils, name="load_file", value=load_file_fixture)


def test_get_version(plugin_fixture: CollectHelmPlugin):
    """
    Test get_version method
    """
    assert len(plugin_fixture.get_version()) > 0


@pytest.mark.parametrize(
    argnames="component", argvalues=["pkg:helm/something/else@1.2.3?repository_url=https://my.repo"], indirect=True
)
def test_collect_helm_url_mismatch(plugin_fixture: CollectHelmPlugin, component: Component):
    """
    Test collect method: purl repository_url not match manifest repo URL
    """
    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == (
        "Purl-specified repository url (https://my.repo) does not match current repo (https://somewhere.com)."
    )
