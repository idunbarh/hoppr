"""
Test module for CollectRawPlugin class
"""

import shutil

# pylint: disable=unused-argument,redefined-outer-name
from pathlib import Path
from test.mock_objects import MockHttpResponse
from typing import List

import pytest
import requests

from hoppr_cyclonedx_models.cyclonedx_1_4 import Component
from pytest import MonkeyPatch

import hoppr
import hoppr.net
import hoppr.plugin_utils

from hoppr.core_plugins import collect_raw_plugin
from hoppr.core_plugins.collect_raw_plugin import CollectRawPlugin
from hoppr.result import Result


@pytest.fixture(scope="function", params=[dict(plugin_class=CollectRawPlugin)])
def plugin_fixture(plugin_fixture: CollectRawPlugin) -> CollectRawPlugin:
    """
    Override and parametrize plugin_fixture to return CollectRawPlugin
    """

    return plugin_fixture


@pytest.fixture(name="component")
def component_fixture() -> Component:
    """
    Test Component fixture
    """
    return Component(name="TestComponent", purl="pkg:generic/something/else@1.2.3", type="file")  # type: ignore


def get_repos(*args, **kwargs) -> List[str]:
    """
    Mock _get_repos method
    """
    return ["https://somewhere.com"]


def get_file_repo(*args, **kwargs) -> List[str]:
    """
    Mock _get_repos method but with a file scheme
    """
    return ["file://somewhere.com"]


def test_collector_raw_url(plugin_fixture: CollectRawPlugin, component: Component, monkeypatch: MonkeyPatch):
    """
    Test raw collector successful run with a given url
    """

    def download(*args, **kwargs) -> MockHttpResponse:
        return MockHttpResponse(200, content="mocked content")

    monkeypatch.setattr(target=CollectRawPlugin, name="_get_repos", value=get_repos)
    monkeypatch.setattr(
        target=collect_raw_plugin,
        name="download_file",
        value=download,
    )

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"


def test_collector_raw_download_file_fail(
    plugin_fixture: CollectRawPlugin, component: Component, monkeypatch: MonkeyPatch
):
    """
    Test raw collector fail download
    """

    def download(*args, **kwargs) -> MockHttpResponse:
        return MockHttpResponse(404, reason="mocked download fail message")

    monkeypatch.setattr(target=CollectRawPlugin, name="_get_repos", value=get_repos)
    monkeypatch.setattr(
        target=collect_raw_plugin,
        name="download_file",
        value=download,
    )

    simulated_result = download()

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == f"HTTP Status Code: {simulated_result.status_code}; {simulated_result.reason}"


def test_collector_raw_file(plugin_fixture: CollectRawPlugin, component: Component, monkeypatch: MonkeyPatch):

    """
    Test raw collector successful run given a file
    """

    copy_mock_output = "some_destination"

    def is_file(*args, **kwargs) -> bool:
        return True

    def get_request(*args, **kwargs) -> MockHttpResponse:
        return MockHttpResponse(200, content="")

    monkeypatch.setattr(target=CollectRawPlugin, name="_get_repos", value=get_file_repo)
    monkeypatch.setattr(target=Path, name="is_file", value=is_file)
    monkeypatch.setattr(target=requests, name="get", value=get_request)
    monkeypatch.setattr(target=shutil, name="copy", value=lambda src, dst: copy_mock_output)

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"


def test_collector_raw_file_not_found(plugin_fixture: CollectRawPlugin, component: Component, monkeypatch: MonkeyPatch):
    """
    Test raw collector when file cannot be found
    """

    def is_file(*args, **kwargs) -> bool:
        return False

    def get_request(*args, **kwargs) -> MockHttpResponse:
        return MockHttpResponse(404, content="")

    monkeypatch.setattr(target=CollectRawPlugin, name="_get_repos", value=get_file_repo)
    monkeypatch.setattr(target=Path, name="is_file", value=is_file)
    monkeypatch.setattr(target=requests, name="get", value=get_request)
    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"


def test_collect_git_url_mismatch(plugin_fixture: CollectRawPlugin, component: Component, monkeypatch: MonkeyPatch):
    """
    Test raw collector when component's repo and the collectors repo do not match
    """

    bad_comp = Component(
        name="TestComponent",
        purl="pkg:generic/something/else@1.2.3?repository_url=my.repo",
        type="file",  # type: ignore
    )

    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=get_repos)
    collect_result = plugin_fixture.process_component(bad_comp)

    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"

    assert collect_result.message == (
        "Purl-specified repository url (my.repo) does not match current repo (https://somewhere.com)."
    )


def test_get_version(plugin_fixture: CollectRawPlugin):
    """
    Test raw collector has version
    """
    assert len(plugin_fixture.get_version()) > 0
