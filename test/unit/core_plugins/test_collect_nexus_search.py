"""
Unit tests for the Nexus Search Collector
"""
# pylint: disable=protected-access

import multiprocessing
from pathlib import Path
from test.mock_objects import MockHttpResponse
import urllib
from packageurl import PackageURL

from hoppr_cyclonedx_models.cyclonedx_1_4 import Component
from hoppr.core_plugins.collect_nexus_search import CollectNexusSearch
from hoppr.configs.credentials import Credentials
from hoppr.result import Result
from hoppr.context import Context
from hoppr.hoppr_types.cred_object import CredObject


def _create_test_plugin(config=None):  # pylint: disable=duplicate-code
    context = Context(
        manifest="MANIFEST",
        collect_root_dir="COLLECTION_DIR",
        consolidated_sbom="BOM",
        delivered_sbom="BOM",
        retry_wait_seconds=1,
        max_processes=3,
        logfile_lock=multiprocessing.Manager().RLock(),
    )
    my_plugin = CollectNexusSearch(context=context, config=config)
    return my_plugin


def test_get_version():
    """
    Test the get_version method
    """
    my_plugin = _create_test_plugin()
    assert len(my_plugin.get_version()) > 0


def _create_test_component(name="test_component", purl_type="rpm", version="0.1.2"):
    purl = f"pkg:{purl_type}/{name}@{version}"
    return Component(name=name, purl=purl, type="file")


def test_collect_nexus_success(mocker):
    """
    Test a successful call to collect_nexus
    """
    mocker.patch.object(CollectNexusSearch, "_get_repos", return_value=["https://mock_nexus.com"])
    mocker.patch.object(Credentials, "find_credentials", return_value=CredObject("user", "pw"))
    mocker.patch.object(CollectNexusSearch, "is_nexus_instance", return_value=True)
    mocker.patch.object(
        CollectNexusSearch, "get_download_urls", return_value=["https://mock_nexus.com/repository/test/testobj.txt"]
    )
    mocker.patch(
        "hoppr.core_plugins.collect_nexus_search.download_file",
        return_value=MockHttpResponse(200, content="mocked content"),
    )

    my_plugin = _create_test_plugin(config={'purl_types': ['rpm', 'pip']})
    comp = _create_test_component()

    collect_result = my_plugin.process_component(comp)
    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"


def test_collect_nexus_not_nexus(mocker):
    """
    Test calling collect_nexus on a non-nexus repository
    """
    mocker.patch.object(CollectNexusSearch, "_get_repos", return_value=["https://mock_nexus.com"])
    mocker.patch.object(CollectNexusSearch, "is_nexus_instance", return_value=False)

    my_plugin = _create_test_plugin()
    comp = _create_test_component()

    collect_result = my_plugin.process_component(comp)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == "https://mock_nexus.com is not a Nexus instance"


def test_collect_nexus_git_not_supported(mocker):
    """
    Test calling collect_nexus for a git purl
    """
    mocker.patch.object(CollectNexusSearch, "_get_repos", return_value=["https://mock_nexus.com"])
    mocker.patch.object(CollectNexusSearch, "is_nexus_instance", return_value=True)

    my_plugin = _create_test_plugin()
    comp = _create_test_component(purl_type="git")

    collect_result = my_plugin.process_component(comp)
    assert collect_result.is_skip(), f"Expected SKIP result, got {collect_result}"
    assert collect_result.message == "Class CollectNexusSearch does not support purl type git"


def test_collect_nexus_bad_purl_repo_url(mocker):
    """
    Test calling collect_nexus with bad url in the purl
    """
    mocker.patch.object(CollectNexusSearch, "_get_repos", return_value=["https://mock_nexus.com"])
    mocker.patch.object(CollectNexusSearch, "is_nexus_instance", return_value=True)
    mocker.patch.object(CollectNexusSearch, "check_purl_specified_url", return_value=Result.fail("mock fail"))

    my_plugin = _create_test_plugin()
    comp = _create_test_component()

    collect_result = my_plugin.process_component(comp)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == "mock fail"


def test_collect_nexus_nothing_found(mocker):
    """
    Test calling collect_nexus with no artifact found
    """
    mocker.patch.object(CollectNexusSearch, "_get_repos", return_value=["https://mock_nexus.com"])
    mocker.patch.object(CollectNexusSearch, "is_nexus_instance", return_value=True)
    mocker.patch.object(CollectNexusSearch, "get_download_urls", return_value=[])
    mocker.patch(
        "hoppr.core_plugins.collect_nexus_search.download_file",
        return_value=MockHttpResponse(200, content="mocked content"),
    )

    my_plugin = _create_test_plugin(config={'purl_types': ['rpm', 'pip']})
    comp = _create_test_component()

    collect_result = my_plugin.process_component(comp)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert (
        collect_result.message
        == "No artifacts found in Nexus instance https://mock_nexus.com for purl pkg:rpm/test_component@0.1.2"
    )


def test_collect_nexus_download_fail(mocker):
    """
    Test calling collect_nexus with a failure on download
    """
    mocker.patch.object(CollectNexusSearch, "_get_repos", return_value=["https://mock_nexus.com"])
    mocker.patch.object(CollectNexusSearch, "is_nexus_instance", return_value=True)
    mocker.patch.object(
        CollectNexusSearch, "get_download_urls", return_value=["https://mock_nexus.com/repository/test/testobj.txt"]
    )
    mocker.patch(
        "hoppr.core_plugins.collect_nexus_search.download_file", return_value=MockHttpResponse(404, content="not found")
    )

    my_plugin = _create_test_plugin(config={'purl_types': ['rpm', 'pip']})
    comp = _create_test_component()

    collect_result = my_plugin.process_component(comp)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == "HTTP Status Code: 404; "


def test_directory_for_nexus():
    """
    Test the directory_for_nexus method
    """
    repo_url = "https://my-nexus.com/repository/my_repo/the_path/file.txt"
    comp = _create_test_plugin()

    purl = PackageURL.from_string("pkg:docker/namespace/image@1.2.3")
    assert comp._directory_for_nexus(purl, repo_url) == Path(
        f"COLLECTION_DIR/docker/{urllib.parse.quote_plus('https://my-nexus.com/repository/my_repo')}/the_path"
    )

    purl = PackageURL.from_string("pkg:pypi/namespace/module.py@1.2.3")
    assert comp._directory_for_nexus(purl, repo_url) == Path(
        f"COLLECTION_DIR/pypi/{urllib.parse.quote_plus('https://my-nexus.com/repository/my_repo')}/module.py_1.2.3"
    )


def test_is_nexus(mocker):
    """
    Test the is_nexus method on success
    """
    mocker.patch("hoppr.core_plugins.collect_nexus_search.sleep")
    mock_request_get = mocker.patch("requests.get")
    mock_request_get.side_effect = [MockHttpResponse(500, content="network issue"), MockHttpResponse(200)]

    creds = CredObject("user", "token")

    assert CollectNexusSearch.is_nexus_instance("http://my-nexus.com", creds)


def test_is_not_nexus(mocker):
    """
    Test the is_nexus method on failure
    """
    mocker.patch("hoppr.core_plugins.collect_nexus_search.sleep")
    mocker.patch("requests.get", return_value=MockHttpResponse(404, content="not found"))

    creds = CredObject("user", "token")

    assert not CollectNexusSearch.is_nexus_instance("http://my-nexus.com", creds)


def test_is_nexus_multifail(mocker):
    """
    Test the is_nexus method with network failures
    """
    mocker.patch("hoppr.core_plugins.collect_nexus_search.sleep")
    mocker.patch("requests.get", return_value=MockHttpResponse(500, content="network issue"))

    creds = CredObject("user", "token")

    assert not CollectNexusSearch.is_nexus_instance("http://my-nexus.com", creds)


def test_get_download_urls(mocker):
    """
    Test the get_download_urls method
    """
    search_result = '{"items": [{ "downloadUrl": "TEST_DOWNLOAD_URL"}]}'
    mocker.patch("requests.get", return_value=MockHttpResponse(200, content=search_result))

    purl = PackageURL.from_string("pkg:generic/namespace/file.txt@1.2.3")
    assert CollectNexusSearch.get_download_urls(purl, "http://my-nexus.com") == ['TEST_DOWNLOAD_URL']

    purl = PackageURL.from_string("pkg:rpm/namespace/file.txt@1.2.3")
    assert CollectNexusSearch.get_download_urls(purl, "http://my-nexus.com") == ['TEST_DOWNLOAD_URL']

    purl = PackageURL.from_string("pkg:maven/namespace/file.txt@1.2.3")
    assert CollectNexusSearch.get_download_urls(purl, "http://my-nexus.com") == [
        'TEST_DOWNLOAD_URL',
        'TEST_DOWNLOAD_URL',
        'TEST_DOWNLOAD_URL',
    ]
