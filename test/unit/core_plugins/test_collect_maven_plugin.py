"""
Test module for CollectMavenPlugin class
"""

# pylint: disable=protected-access
# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument

from __future__ import annotations

from pathlib import Path
from subprocess import CompletedProcess

import pytest

from hoppr_cyclonedx_models.cyclonedx_1_4 import Component
from pytest import FixtureRequest, MonkeyPatch

import hoppr.plugin_utils

from hoppr.configs.credentials import Credentials
from hoppr.context import Context
from hoppr.core_plugins.collect_maven_plugin import CollectMavenPlugin
from hoppr.hoppr_types.cred_object import CredObject
from hoppr.hoppr_types.manifest_file_content import Repository
from hoppr.hoppr_types.purl_type import PurlType
from hoppr.result import Result

SETTINGS_XML = """<settings>
    <profiles>
        <profile>
            <id>hoppr-test-profile</id>
            <repositories>
                <repository>
                    <id>hoppr-test-repo-1</id>
                    <name>Hoppr Test 1</name>
                    <url>https://somewhere.com</url>
                </repository>
                <repository>
                    <id>hoppr-test-repo-2</id>
                    <name>Hoppr Test 2</name>
                    <url>https://somewhere.else.com</url>
                </repository>
            </repositories>
        </profile>
    </profiles>
</settings>
"""


@pytest.fixture(name="component")
def component_fixture(request: FixtureRequest):
    """
    Test Component fixture
    """
    purl = getattr(request, "param", "pkg:maven/something/else@1.2.3")
    return Component(name="TestMavenComponent", purl=purl, type="file")  # type: ignore


@pytest.fixture(scope="function")
def context_fixture(context_fixture: Context, monkeypatch: MonkeyPatch) -> Context:
    """
    Test Context fixture
    """
    monkeypatch.setattr(target=Path, name="is_file", value=lambda self: True)
    monkeypatch.setattr(target=Path, name="read_text", value=lambda self, encoding: SETTINGS_XML)

    return context_fixture


@pytest.fixture(scope="function", params=[dict(plugin_class=CollectMavenPlugin)])
def plugin_fixture(
    plugin_fixture: CollectMavenPlugin, config_fixture: dict[str, str], monkeypatch: MonkeyPatch, tmp_path: Path
) -> CollectMavenPlugin:
    """
    Override and parametrize plugin_fixture to return CollectDnfPlugin
    """
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=lambda comp: ["https://somewhere.com"])

    plugin_fixture.context.collect_root_dir = str(tmp_path)
    plugin_fixture.config = dict(maven_command="mvn", maven_opts=["-D1", "-D2"])
    plugin_fixture.context.manifest.consolidated_repositories = {
        PurlType.MAVEN: [Repository(url="https://somewhere.com", description="")]
    }

    return plugin_fixture


@pytest.mark.parametrize(
    argnames="completed_process_fixture", argvalues=[dict(returncode=0), dict(returncode=1)], indirect=True
)
def test_collect_maven(
    # pylint: disable=too-many-arguments
    plugin_fixture: CollectMavenPlugin,
    component: Component,
    completed_process_fixture: CompletedProcess,
    find_credentials_fixture: CredObject,
    run_command_fixture: CompletedProcess,
    monkeypatch: MonkeyPatch,
):
    """
    Test collect method
    """
    monkeypatch.setattr(target=Credentials, name="find_credentials", value=find_credentials_fixture)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)

    if completed_process_fixture.returncode == 0:
        assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"
    else:
        assert collect_result.is_fail()


@pytest.mark.parametrize(
    argnames="component", argvalues=["pkg:maven/something/else@1.2.3?repository_url=https://my.repo"], indirect=True
)
def test_collect_maven_url_mismatch(plugin_fixture: CollectMavenPlugin, component: Component):
    """
    Test collect method: purl repository_url not match manifest repo URL
    """
    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == (
        "Purl-specified repository url (https://my.repo) does not match current repo (https://somewhere.com)."
    )


def test_get_version(plugin_fixture: CollectMavenPlugin):
    """
    Test get_version method
    """
    assert len(plugin_fixture.get_version()) > 0


@pytest.mark.parametrize(argnames="context_fixture", argvalues=[dict(strict_repos=False)], indirect=True)
def test_maven_no_strict(plugin_fixture: CollectMavenPlugin, context_fixture: Context):
    """
    Test collect method: --no-strict flag
    """
    assert plugin_fixture.context.strict_repos is False
