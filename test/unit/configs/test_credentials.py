"""
Test module for Credentials class
"""

# pylint: disable=missing-function-docstring

import test

from pathlib import Path

import pytest

from pytest import MonkeyPatch

from hoppr.configs.credentials import Credentials
from hoppr.exceptions import HopprCredentialsError
from hoppr.hoppr_types.credentials_file_content import CredentialRequiredService


@pytest.fixture(autouse=True)
def credentials_fixture():
    Credentials.load_file(Path(test.__file__).parent / "resources" / "credential" / "cred-test.yml")


def test_credentials_load():
    credential_data = Credentials.get_content()

    assert credential_data
    assert credential_data.kind
    assert credential_data.metadata

    assert credential_data.kind == "Credentials"
    assert credential_data.metadata.name == "Registry Credentials"
    assert credential_data.metadata.version == "v1"
    assert credential_data.metadata.description == "Sample credentials file"

    svc1 = CredentialRequiredService(url='gitlab.com', user='infra-pipeline-auto', user_env=None, pass_env='GITLAB_PW')
    svc2 = CredentialRequiredService(url='some-site.com', user='pipeline-bot', user_env=None, pass_env='SOME_TOKEN')
    svc3 = CredentialRequiredService(url='registry-test.com', user='cis-gitlab', user_env=None, pass_env='DOCKER_PW')

    assert svc1 in credential_data.credential_required_services
    assert svc2 in credential_data.credential_required_services
    assert svc3 in credential_data.credential_required_services


def test_credentials_find_success(monkeypatch: MonkeyPatch):
    with monkeypatch.context() as patch:
        patch.setenv(name="GITLAB_PW", value="GITLAB_ENV_TEST_PASSWORD")

        personal_cred = Credentials.find_credentials("gitlab.com")

        assert personal_cred
        assert personal_cred.username == "infra-pipeline-auto"
        assert personal_cred.password == "GITLAB_ENV_TEST_PASSWORD"


def test_credentials_find_failed(monkeypatch: MonkeyPatch):
    with monkeypatch.context() as patch:
        patch.delenv(name="GITLAB_PW", raising=False)

        with pytest.raises(expected_exception=HopprCredentialsError):
            personal_cred = Credentials.find_credentials("gitlab.com")
            assert personal_cred is None


def test_credentials_no_required_service():
    personal_cred = Credentials.find_credentials("some_site.com")
    assert personal_cred is None
