"""
Test module for Transfer class
"""

import test

from pathlib import Path

from hoppr.configs.transfer import Transfer


def test_success():
    """
    Test load_file method
    """
    transfer_config = Transfer.load_file(Path(test.__file__).parent / "resources" / "transfer" / "transfer-test.yml")

    assert transfer_config
    assert transfer_config.content
    assert transfer_config.content.kind == "Transfer"

    assert len(transfer_config.content.stages) == 2
    assert transfer_config.content.stages[0].name == "Collect"
    assert transfer_config.content.stages[1].name == "Bundle"
    assert transfer_config.content.stages[0].plugins[0].name == "hoppr.core_plugins.collect_apt_plugin"
    assert transfer_config.content.stages[1].plugins[0].config["tarfile_name"] == "~/tarfile.tar.gz"
    assert transfer_config.content.max_processes == 3
