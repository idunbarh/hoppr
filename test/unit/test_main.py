"""
Test module for main module
"""

# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument

import test

from pathlib import Path

import pytest

from click.exceptions import Exit
from pytest import MonkeyPatch

import hoppr.main
import hoppr.processor

from hoppr.configs.credentials import Credentials
from hoppr.configs.manifest import Manifest
from hoppr.configs.transfer import Transfer
from hoppr.processor import HopprProcessor
from hoppr.result import Result


class MockProcessor:  # pylint: disable=too-few-public-methods
    """
    Mock HopprProcessor
    """

    metadata_files: list[str] = []

    def __init__(self, *args, **kwargs):
        super().__init__()

    def run(self, *args, **kwargs) -> Result:
        """
        Mock run method
        """
        return Result.fail()


@pytest.mark.parametrize(argnames="expected", argvalues=[Result.fail, Result.success])
def test_bundle(monkeypatch: MonkeyPatch, expected: Result):
    """
    Test bundle method
    """

    monkeypatch.setattr(target=Manifest, name="load_file", value=lambda file: None)
    monkeypatch.setattr(target=Credentials, name="load_file", value=lambda file: None)
    monkeypatch.setattr(target=Transfer, name="load_file", value=lambda file: None)

    monkeypatch.setattr(target=hoppr.processor, name="HopprProcessor", value=lambda *args, **kwargs: None)
    monkeypatch.setattr(target=HopprProcessor, name="run", value=lambda *args, **kwargs: expected)

    if expected is Result.fail():
        with pytest.raises(SystemExit) as pytest_wrapped_e:
            hoppr.main.bundle(
                Path(test.__file__).parent / "resources" / "manifest" / "manifest.yml",
                Path(test.__file__).parent / "resources" / "credential" / "cred-test.yml",
                Path(test.__file__).parent / "resources" / "transfer" / "transfer-test.yml",
                Path("mylog.txt"),
            )

        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 1


def test_bundle_no_key(monkeypatch: MonkeyPatch):
    """
    Test bundle method without a key
    """

    monkeypatch.setattr(target=Manifest, name="load_file", value=lambda file: None)
    monkeypatch.setattr(target=Credentials, name="load_file", value=lambda file: None)
    monkeypatch.setattr(target=Transfer, name="load_file", value=lambda file: None)

    monkeypatch.setattr(target=hoppr.processor, name="HopprProcessor", value=lambda *args, **kwargs: None)
    monkeypatch.setattr(target=HopprProcessor, name="run", value=lambda *args, **kwargs: Result.success())

    with pytest.raises(Exit) as pytest_wrapped_e:
        hoppr.main.bundle(
            Path(test.__file__).parent / "resources" / "manifest" / "manifest.yml",
            Path(test.__file__).parent / "resources" / "credential" / "cred-test.yml",
            Path(test.__file__).parent / "resources" / "transfer" / "transfer-test.yml",
            Path("mylog.txt"),
            create_attestations=True,
        )
    assert pytest_wrapped_e.type == Exit


@pytest.mark.parametrize(argnames="func_key_prompt", argvalues=[True, False])
def test_bundle_attest_prompt(monkeypatch: MonkeyPatch, func_key_prompt: bool):
    """
    Test bundle method with and without prompt
    """

    monkeypatch.setattr(target=Manifest, name="load_file", value=lambda file: None)
    monkeypatch.setattr(target=Credentials, name="load_file", value=lambda file: None)
    monkeypatch.setattr(target=Transfer, name="load_file", value=lambda file: None)

    monkeypatch.setattr(target=hoppr.processor, name="HopprProcessor", value=lambda *args, **kwargs: None)
    monkeypatch.setattr(target=HopprProcessor, name="run", value=lambda *args, **kwargs: Result.success())
    monkeypatch.setattr(target=HopprProcessor, name="__new__", value=MockProcessor)

    monkeypatch.setattr(target=hoppr.main, name="prompt", value=lambda *args, **kwargs: "5678")

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        hoppr.main.bundle(
            Path(test.__file__).parent / "resources" / "manifest" / "manifest.yml",
            Path(test.__file__).parent / "resources" / "credential" / "cred-test.yml",
            Path(test.__file__).parent / "resources" / "transfer" / "transfer-test.yml",
            log_file=Path("mylog.txt"),
            create_attestations=True,
            functionary_key_path=Path("mylog.txt"),
            functionary_key_prompt=func_key_prompt,
        )
    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 1


@pytest.mark.parametrize(argnames="will_generate_prompt", argvalues=[True, False])
def test_generate_layout_prompt(monkeypatch: MonkeyPatch, will_generate_prompt: bool):
    """
    Test generate_layout fuction with and without prompt
    """

    product_owner_path = Path("product_owner_key")
    functionary_path = Path("functionary_key")

    monkeypatch.setattr(target=Transfer, name="load_file", value=lambda file: None)

    monkeypatch.setattr(target=hoppr.processor, name="HopprProcessor", value=lambda *args, **kwargs: None)
    monkeypatch.setattr(target=HopprProcessor, name="run", value=lambda *args, **kwargs: Result.success())
    monkeypatch.setattr(target=HopprProcessor, name="__new__", value=MockProcessor)

    monkeypatch.setattr(target=hoppr.main, name="prompt", value=lambda *args, **kwargs: "5678")
    monkeypatch.setattr(target=hoppr.main, name="generate_in_toto_layout", value=lambda *args, **kwargs: None)

    hoppr.main.generate_layout(
        Path(test.__file__).parent / "resources" / "transfer" / "transfer-test.yml",
        product_owner_path,
        functionary_path,
        will_generate_prompt,
        "1234",
    )


def test_validate():
    """
    Test validate method
    """

    hoppr.main.validate(
        [Path("test", "resources", "manifest", "unit", "manifest.yml").resolve()],
        Path("test", "resources", "credential", "cred-test.yml").resolve(),
        Path("test", "resources", "transfer", "transfer-test.yml").resolve(),
    )
