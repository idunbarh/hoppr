"""
Test module for manifest pydantic models
"""
from __future__ import annotations

import test

from pathlib import Path
from typing import TYPE_CHECKING, Callable, Type, TypeVar

import pytest

from pytest import FixtureRequest, MonkeyPatch
from requests import HTTPError
from ruamel.yaml import YAML

import hoppr
import hoppr.net
import hoppr.oci_artifacts
import hoppr.utils

from hoppr.exceptions import HopprLoadDataError
from hoppr.models.manifest import (
    IncludeRef,
    Includes,
    LocalFile,
    Manifest,
    ManifestFile,
    OciFile,
    Sbom,
    SbomRef,
    Sboms,
    UrlFile,
)

if TYPE_CHECKING:
    from pydantic.typing import DictStrAny

# pylint: disable=protected-access,unused-argument

ExpectedException = TypeVar("ExpectedException", bound=BaseException)


@pytest.fixture(name="includes", scope="function")
def includes_fixture(request: FixtureRequest) -> list[DictStrAny]:
    """
    Fixture to return `includes` object
    """
    if hasattr(request, "param"):
        return request.param

    return [
        {"local": "test/resources/manifest/manifest.yml"},
        {"local": "test/resources/manifest/unit/manifest.yml"},
        {"url": "https://test.hoppr.com/helm/manifest.yml"},
        {"url": "https://test.hoppr.com/pypi/manifest.yml"},
    ]


@pytest.fixture(name="load_url_fixture", scope="function")
def _load_url_fixture(request: FixtureRequest) -> Callable[..., object]:
    """
    Fixture to patch hoppr.net.load_url
    """

    def _load_url(url: str):
        if request.param == "HTTPError":
            raise HTTPError

        if request.param == "HopprLoadDataError":
            raise HopprLoadDataError

        return request.param

    return _load_url


@pytest.fixture(name="manifest_dict", scope="function")
def manifest_dict_fixture(includes: list[DictStrAny], sboms: list[DictStrAny]):
    """
    Fixture to return dict representation of a ManifestFile object
    """
    return {
        "kind": "Manifest",
        "metadata": {
            "name": "Unit Test Manifest",
            "version": "0.1.0",
            "description": "Manifest for unit tests",
        },
        "schemaVersion": "v1",
        "repositories": {
            "deb": [
                {"url": "https://apt.repo.alpha.com"},
                {"url": "https://apt.repo.beta.com"},
            ],
            "generic": [{"url": "file://"}],
            "pypi": [{"url": "https://pypi.hoppr.com/repositories/pypi"}],
            "rpm": [
                {"url": "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os"},
                {"url": "https://rpm.hoppr.com/dl/8/BaseOS/x86_64/os"},
                {"url": "https://rpm.hoppr.com/dl/8/PowerTools/x86_64/os"},
            ],
        },
        "includes": includes or [],
        "sboms": sboms or [],
    }


@pytest.fixture(name="manifest", scope="function")
def manifest_fixture(manifest_dict: DictStrAny, tmp_path: Path) -> ManifestFile:
    """
    Fixture to return ManifestFile object
    """
    manifest_path = tmp_path / "manifest.yml"
    yaml = YAML(typ="safe", pure=True)

    with manifest_path.open(mode="w+", encoding="utf-8") as stream:
        yaml.dump(data=manifest_dict, stream=stream)

    # Add includes to loaded_manifests to prevent parsing test files that don't exist
    for include in manifest_dict.get("includes", []):
        if "local" in include:
            ManifestFile.loaded_manifests.add(LocalFile(local=manifest_path.parent / include["local"]))
        elif "url" in include:
            ManifestFile.loaded_manifests.add(UrlFile(url=include["url"]))

    # Use parse_file method so relative paths are resolved
    manifest_file = ManifestFile.parse_file(manifest_path)

    return manifest_file


@pytest.fixture(name="sboms", scope="function")
def sboms_fixture(request: FixtureRequest) -> list[DictStrAny]:
    """
    Fixture to return `sboms` object
    """
    if hasattr(request, "param"):
        return request.param

    return [
        {"local": "../../bom/unit_bom1_mini.json"},
        {"local": "../../bom/unit_bom2_mini.json"},
        {"url": "http://test.hoppr.com/sbom_1.json"},
        {"url": "http://test.hoppr.com/sbom_2.json"},
    ]


def test_includes_iterator():
    """
    Test Includes.__iter__ method
    """
    includes = Includes(
        __root__=[
            IncludeRef(__root__=LocalFile(local=Path("test/resources/manifest/unit/manifest.yml"))),
            IncludeRef(__root__=UrlFile(url="https://test.hoppr.com/helm/manifest.yml")),
        ]
    )

    assert repr(includes) == (
        "Includes([LocalFile(local=PosixPath('test/resources/manifest/unit/manifest.yml')), "
        "UrlFile(url=HttpUrl('https://test.hoppr.com/helm/manifest.yml', ))])"
    )

    # Tests __iter__ and __getitem__ methods
    for idx, include in enumerate(includes):
        assert repr(include) == repr(includes[idx])  # pylint: disable=unnecessary-list-index-lookup
        assert repr(include).startswith(
            (
                "LocalFile(local=PosixPath(",
                "UrlFile(url=HttpUrl(",
            )
        )


@pytest.mark.parametrize(
    argnames="loaded_manifests",
    argvalues=[
        set(),
        set([LocalFile(local=Path("test/resources/manifest/manifest.yml"))]),
        set([UrlFile(url="https://test.hoppr.com/helm/manifest.yml")]),
    ],
)
def test_manifest__load_local_include(
    manifest: ManifestFile,
    manifest_dict: DictStrAny,
    loaded_manifests: set[LocalFile | UrlFile],
    monkeypatch: MonkeyPatch,
    tmp_path: Path,
):
    """
    Test Manifest._load_local_include method
    """
    includes_refs = [
        None,
        LocalFile(local=Path("test/resources/manifest/manifest.yml")),
        UrlFile(url="https://test.hoppr.com/helm/manifest.yml"),
        OciFile(oci="oci://registry.hoppr.com/manifest.yml"),
    ]

    with monkeypatch.context() as patch:
        patch.setattr(target=manifest, name="includes", value=includes_refs)
        patch.setattr(target=hoppr.net, name="load_url", value=lambda url: manifest_dict)
        patch.setattr(target=Manifest, name="load", value=lambda source: manifest)
        patch.setattr(target=Manifest, name="loaded_manifests", value=loaded_manifests)

        Manifest._load_local_include(manifest_dir=tmp_path, local_include=manifest)

    assert OciFile(oci="oci://registry.hoppr.com/manifest.yml") not in Manifest.loaded_manifests


def test_manifest__load_local_include_already_loaded(manifest: ManifestFile, monkeypatch: MonkeyPatch, tmp_path: Path):
    """
    Test Manifest._load_local_include method with a local manifest already loaded
    """
    include_refs = [LocalFile(local=tmp_path / "test/resources/manifest/manifest.yml")]

    with monkeypatch.context() as patch:
        patch.setattr(target=manifest, name="includes", value=include_refs)
        patch.setattr(target=Manifest, name="loaded_manifests", value=include_refs)

        Manifest._load_local_include(manifest_dir=tmp_path, local_include=manifest)


@pytest.mark.parametrize(
    argnames="loaded_manifests",
    argvalues=[
        set(),
        set([LocalFile(local=Path("test/resources/manifest/manifest.yml"))]),
        set([UrlFile(url="https://test.hoppr.com/helm/manifest.yml")]),
    ],
)
def test_manifest__load_url_include(
    manifest: ManifestFile,
    manifest_dict: DictStrAny,
    loaded_manifests: set[LocalFile | UrlFile],
    monkeypatch: MonkeyPatch,
):
    """
    Test Manifest._load_url_include method
    """
    includes_refs = [
        None,
        LocalFile(local=Path("test/resources/manifest/manifest.yml")),
        UrlFile(url="https://test.hoppr.com/helm/manifest.yml"),
        OciFile(oci="oci://registry.hoppr.com/manifest.yml"),
    ]

    with monkeypatch.context() as patch:
        patch.setattr(target=manifest, name="includes", value=includes_refs)
        patch.setattr(target=hoppr.net, name="load_url", value=lambda url: manifest_dict)
        patch.setattr(target=Manifest, name="load", value=lambda source: manifest)
        patch.setattr(target=Manifest, name="loaded_manifests", value=loaded_manifests)

        Manifest._load_url_include(manifest)

    assert OciFile(oci="oci://registry.hoppr.com/manifest.yml") not in Manifest.loaded_manifests


def test_manifest__load_url_include_bad_content(manifest: ManifestFile, monkeypatch: MonkeyPatch):
    """
    Test Manifest._load_url_include method with bad file content
    """
    with monkeypatch.context() as patch:
        patch.setattr(target=manifest, name="includes", value=[UrlFile(url="https://test.hoppr.com/helm/manifest.yml")])
        patch.setattr(target=hoppr.net, name="load_url", value=lambda url: [])
        patch.setattr(target=Manifest, name="loaded_manifests", value=set())

        Manifest._load_url_include(manifest)

        assert len(Manifest.loaded_manifests) == 0


def test_manifest_file_parse_file():
    """
    Test ManifestFile.parse_file method
    """
    manifest_path = Path(test.__file__).parent / "resources" / "manifest" / "unit" / "manifest.yml"
    manifest = ManifestFile.parse_file(manifest_path)

    assert len(manifest.includes) == 1


def test_manifest_file_parse_file_fail(monkeypatch: MonkeyPatch):
    """
    Test ManifestFile.parse_file method fail to load file
    """
    manifest_path = Path(test.__file__).parent / "resources" / "manifest" / "unit" / "manifest.yml"

    monkeypatch.setattr(target=hoppr.utils, name="load_file", value=lambda file: None)

    with pytest.raises(expected_exception=TypeError) as pytest_exception:
        ManifestFile.parse_file(manifest_path)

    assert pytest_exception.value.args[0] == "Local file content was not loaded as dictionary"


@pytest.mark.parametrize(argnames=["includes", "sboms"], argvalues=[([], [])], indirect=True)
def test_manifest_file_parse_obj(manifest: ManifestFile, monkeypatch: MonkeyPatch):
    """
    Test ManifestFile.parse_obj method
    """
    with monkeypatch.context() as patch:
        patch.setattr(target=ManifestFile, name="loaded_manifests", value=[])
        manifest_dict = manifest.dict(by_alias=True)

        manifest_dict["includes"] = [
            {"local": "test/resources/manifest/manifest.yml"},
            {"local": "test/resources/manifest/unit/manifest.yml"},
        ]

        manifest_dict["sboms"] = [
            {"local": "../../bom/unit_bom1_mini.json"},
            {"local": "../../bom/unit_bom2_mini.json"},
        ]

        manifest_obj = ManifestFile.parse_obj(manifest_dict)

    # Test that local refs were removed
    assert len(manifest_obj.includes) == 0
    assert len(manifest_obj.sboms) == 0


@pytest.mark.parametrize(
    argnames="includes_refs",
    argvalues=[
        [OciFile(oci="https://registry.hoppr.com"), UrlFile(url="https://test.hoppr.com/manifest.yml")],
    ],
)
def test_manifest_file_includes_validation_error(
    manifest: ManifestFile, includes_refs: list[LocalFile | UrlFile], monkeypatch: MonkeyPatch
):
    """
    Test pydantic validator for Manifest.includes attribute raises TypeError
    """
    with monkeypatch.context() as patch:
        patch.setattr(target=hoppr.utils, name="load_file", value=lambda file: None)
        patch.setattr(target=hoppr.net, name="load_url", value=lambda file: None)
        patch.setattr(target=manifest, name="sboms", value=[])
        patch.setattr(target=ManifestFile, name="loaded_manifests", value=set())

        with pytest.raises(expected_exception=AssertionError) as pytest_exception:
            ManifestFile.load_includes(includes_refs)

        assert pytest_exception.value.args[0] == "URL manifest include was not loaded as dictionary"


def test_manifest_file_includes_validator(manifest: ManifestFile, monkeypatch: MonkeyPatch):
    """
    Test pydantic validator for Manifest.includes attribute
    """
    monkeypatch.setattr(target=hoppr.net, name="load_url", value=lambda url: manifest.dict(by_alias=True))
    monkeypatch.setattr(target=hoppr.utils, name="load_file", value=lambda path: manifest.dict(by_alias=True))

    ManifestFile.load_includes(
        includes=[
            LocalFile(local=Path("../manifest/manifest_1.yml")),
            UrlFile(url="https://test.hoppr.com/manifest_1.yml"),
        ]
    )


@pytest.mark.parametrize(argnames="sboms", argvalues=[[]], indirect=True)
def test_manifest_includes_validator(manifest: ManifestFile, includes: list[DictStrAny], monkeypatch: MonkeyPatch):
    """
    Test pydantic validator for Manifest.includes attribute
    """
    monkeypatch.setattr(target=Manifest, name="parse_obj", value=lambda obj: manifest)
    loaded_includes = Manifest.load_includes(includes)

    assert len(loaded_includes) == 4


@pytest.mark.parametrize(argnames=["includes", "sboms"], argvalues=[([], [])], indirect=True)
def test_manifest_load_dict(manifest_dict: DictStrAny):
    """
    Test Manifest.load method with dict input
    """
    Manifest.load(manifest_dict)


def test_manifest_load_file():
    """
    Test Manifest.load method with Path input
    """
    manifest_path = Path(test.__file__).parent / "resources" / "manifest" / "unit" / "manifest.yml"
    Manifest.load(manifest_path)


@pytest.mark.parametrize(
    argnames=["load_url_fixture", "expected_exception", "expected_message"],
    argvalues=[
        ({}, None, None),
        ([], TypeError, "URL manifest include was not loaded as dictionary"),
        ("HopprLoadDataError", HopprLoadDataError, ""),
        ("HTTPError", HopprLoadDataError, ""),
    ],
    indirect=["load_url_fixture"],
)
def test_manifest_load_url(  # pylint: disable=too-many-arguments
    manifest: ManifestFile,
    manifest_dict: DictStrAny,
    load_url_fixture: Callable[..., object],
    expected_exception: Type[ExpectedException] | None,
    expected_message: str,
    monkeypatch: MonkeyPatch,
):
    """
    Test Manifest.load method with URL input
    """
    manifest_url = "https://test.hoppr.com/manifest.yml"

    with monkeypatch.context() as patch:
        patch.setattr(target=Manifest, name="__new__", value=lambda *args, **kwargs: None)
        patch.setattr(target=Manifest, name="_load_url_include", value=lambda manifest_file: manifest_dict)
        patch.setattr(target=ManifestFile, name="parse_obj", value=lambda obj: manifest)
        patch.setattr(target=hoppr.net, name="load_url", value=load_url_fixture)

        if expected_exception is not None:
            with pytest.raises(expected_exception, match=expected_message) as ex:
                Manifest.load(manifest_url)

            assert ex.type == expected_exception
        else:
            Manifest.load(manifest_url)


@pytest.mark.parametrize(
    argnames=["sboms", "expected_message"],
    argvalues=[
        ([{"local": "local-sbom.json"}], "Local file content was not loaded as dictionary"),
        ([{"url": "https://test.hoppr.com/url-sbom.json"}], "URL SBOM file was not loaded as dictionary"),
    ],
)
def test_manifest_sboms_validation_error(sboms: list[DictStrAny], expected_message: str, monkeypatch: MonkeyPatch):
    """
    Test pydantic validator for Manifest.sboms attribute raises TypeError
    """
    with monkeypatch.context() as patch:
        patch.setattr(target=hoppr.net, name="load_url", value=lambda url: None)
        patch.setattr(target=hoppr.utils, name="load_file", value=lambda path: None)

        with pytest.raises(expected_exception=AssertionError) as pytest_exception:
            Manifest.load_sboms(sboms)

        assert pytest_exception.value.args[0] == expected_message


@pytest.mark.parametrize(
    argnames="sboms",
    argvalues=[
        [{"local": "local-sbom.json"}],
        [{"oci": "oci://test.hoppr.com/oci-sbom.json"}],
        [{"url": "https://test.hoppr.com/url-sbom.json"}],
        [{"invalid": None}],
    ],
    indirect=True,
)
def test_manifest_sboms_validator(sboms: list[DictStrAny], monkeypatch: MonkeyPatch):
    """
    Test pydantic validator for Manifest.sboms attribute
    """
    sbom = Sbom.parse_file(Path(test.__file__).parent / "resources" / "bom" / "unit_bom1_mini.json")
    sbom_dict = sbom.dict(by_alias=True)

    monkeypatch.setattr(target=hoppr.net, name="load_url", value=lambda url: sbom_dict)
    monkeypatch.setattr(target=hoppr.oci_artifacts, name="pull_artifact", value=lambda path: sbom_dict)
    monkeypatch.setattr(target=hoppr.utils, name="load_file", value=lambda path: sbom_dict)

    Manifest.load_sboms(sboms)


def test_sbom_hash():
    """
    Test Sbom.__hash__ method
    """
    sbom = Sbom.parse_file(Path(test.__file__).parent / "resources" / "bom" / "unit_bom1_mini.json")
    assert isinstance(hash(sbom), int)


def test_sboms_iterator():
    """
    Test Sboms.__iter__ method
    """
    sboms = Sboms(
        __root__=[
            SbomRef(__root__=LocalFile(local=Path("../../bom/unit_bom2_mini.json"))),
            SbomRef(__root__=OciFile(oci="https://test.hoppr.com/oci-sbom.json")),
            SbomRef(__root__=UrlFile(url="http://test.hoppr.com/sbom.json")),
        ]
    )

    assert repr(sboms) == (
        "Sboms([LocalFile(local=PosixPath('../../bom/unit_bom2_mini.json')), "
        "OciFile(oci=AnyUrl('https://test.hoppr.com/oci-sbom.json', scheme='https', "
        "host='test.hoppr.com', tld='com', host_type='domain', path='/oci-sbom.json')), "
        "UrlFile(url=HttpUrl('http://test.hoppr.com/sbom.json', ))])"
    )

    # Tests __iter__ and __getitem__ methods
    for idx, sbom in enumerate(sboms):
        assert repr(sbom) == repr(sboms[idx])  # pylint: disable=unnecessary-list-index-lookup
        assert repr(sbom).startswith(
            (
                "LocalFile(local=PosixPath(",
                "OciFile(oci=AnyUrl(",
                "UrlFile(url=HttpUrl(",
            )
        )
