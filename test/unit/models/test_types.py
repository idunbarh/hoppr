"""
Test module for data types pydantic models
"""
from __future__ import annotations

import test

from pathlib import Path

import pytest

from hoppr_cyclonedx_models.cyclonedx_1_4 import CyclonedxSoftwareBillOfMaterialsStandard as Bom

from hoppr.models.types import BomAccess, ComponentCoverage


@pytest.mark.parametrize(
    argnames="access_type",
    argvalues=[
        BomAccess.NO_ACCESS,
        BomAccess.COMPONENT_ACCESS,
        BomAccess.FULL_ACCESS,
    ],
)
def test_bom_access(access_type: BomAccess):
    """
    Test BomAccess enum
    """
    bom = Bom.parse_file(Path(test.__file__).parent / "resources" / "bom" / "unit_bom1_mini.json")

    assert bom.components is not None
    component = bom.components[0]

    if access_type == BomAccess.NO_ACCESS:
        assert access_type.has_access_to(None)

    if access_type == BomAccess.COMPONENT_ACCESS:
        assert access_type.has_access_to(component)

    if access_type == BomAccess.FULL_ACCESS:
        assert access_type.has_access_to(bom)


def test_component_coverage():
    """
    Test ComponentCoverage enum
    """
    assert str(ComponentCoverage.AT_LEAST_ONCE) == "AT_LEAST_ONCE"
    assert ComponentCoverage.AT_LEAST_ONCE.accepts_count(1) is True
    assert ComponentCoverage.AT_LEAST_ONCE.accepts_count(0) is False

    assert str(ComponentCoverage.EXACTLY_ONCE) == "EXACTLY_ONCE"
    assert ComponentCoverage.EXACTLY_ONCE.accepts_count(1) is True
    assert ComponentCoverage.EXACTLY_ONCE.accepts_count(2) is False

    assert str(ComponentCoverage.NO_MORE_THAN_ONCE) == "NO_MORE_THAN_ONCE"
    assert ComponentCoverage.NO_MORE_THAN_ONCE.accepts_count(0) is True
    assert ComponentCoverage.NO_MORE_THAN_ONCE.accepts_count(2) is False

    assert str(ComponentCoverage.OPTIONAL) == "OPTIONAL"
    assert ComponentCoverage.OPTIONAL.accepts_count(0) is True
    assert ComponentCoverage.OPTIONAL.accepts_count(-1) is False
