"""
Define modules to skip when running unit tests
"""
import pytest

pytest.importorskip(modname="hoppr.models.__main__")
