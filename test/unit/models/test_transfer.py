"""
Test module for transfer pydantic models
"""
import test

from pathlib import Path

import pytest

from hoppr.models.transfer import Stage, Transfer, TransferFile

transfer_dict = {
    "kind": "Transfer",
    "metadata": None,
    "schemaVersion": "v1",
    "max_processes": 3,
    "stages": {
        "Collect": {
            "component_coverage": None,
            "plugins": [
                {"name": "hoppr.core_plugins.collect_apt_plugin", "config": None},
                {"name": "hoppr.core_plugins.collect_docker_plugin", "config": None},
                {"name": "hoppr.core_plugins.collect_git_plugin", "config": None},
                {"name": "hoppr.core_plugins.collect_helm_plugin", "config": None},
                {"name": "hoppr.core_plugins.collect_maven_plugin", "config": None},
                {"name": "hoppr.core_plugins.collect_pypi_plugin", "config": None},
                {"name": "hoppr.core_plugins.collect_yum_plugin", "config": None},
                {"name": "hoppr.core_plugins.collect_raw_plugin", "config": None},
            ],
        },
        "Bundle": {
            "component_coverage": None,
            "plugins": [{"name": "hoppr.core_plugins.bundle_tar", "config": {"tarfile_name": "~/tarfile.tar.gz"}}],
        },
    },
}

transfer_file = Path(test.__file__).parent / "resources" / "transfer" / "transfer-test.yml"


def test_get_stage_by_name():
    """
    Test stage indexed lookup
    """

    transfer = TransferFile.parse_file(transfer_file)
    assert isinstance(transfer.stages["Collect"], Stage)


def test_load_transfer_file():
    """
    Test Transfer.load method with Path input
    """
    Transfer.load(transfer_file)


def test_load_transfer_dict():
    """
    Test Transfer.load method with dict input
    """
    Transfer.load(transfer_dict)


def test_load_transfer_wrong_type():
    """
    Test Transfer.load method with wrong input type
    """
    with pytest.raises(expected_exception=TypeError):
        Transfer.load(0)
